<?php
#Name:Ritchey Verify Drive i1 v1
#Description:Check the data retention of a storage device. Returns details as a string on success. Returns "FALSE" on failure.
#Notes:This script is designed for use on linux operating systems, and will not work on Windows. Optional arguments can be "NULL" to skip them in which case they will use default values. Data on the device will be overwritten!
#Arguments:'source' (required) is the path, and name of the device to check. 'string' (optional) is a string which will be used as the basis for generating data. 'display_errors' (optional) indicates if errors should be displayed.
#Arguments (Script Friendly):source:device:required,string:string:optional,display_errors:bool:optional
#Content:
if (function_exists('ritchey_verify_drive_i1_v1') === FALSE){
function ritchey_verify_drive_i1_v1($source, $string = NULL, $display_errors = NULL){
	$errors = array();
	if (@file_exists($source) === FALSE) {
		$errors[] = 'source';
	}
	if ($string === NULL){
		$string = 'Ritchey Verify Drive';
	} else if (@isset($string) === TRUE){
		#Do Nothing
	} else {
		$errors[] = "string";
	}
	if ($display_errors === NULL){
		$display_errors = FALSE;
	} else if ($display_errors === TRUE){
		#Do Nothing
	} else if ($display_errors === FALSE){
		#Do Nothing
	} else {
		$errors[] = "display_errors";
	}
	##Task []
	if (@empty($errors) === TRUE){
		$string2 = $string;
		###Write to device 4096 bytes (4K sector size) at a time
		$handle = @fopen($source, 'a');
		if ($handle === FALSE) {
			$errors[] = 'handle';
			goto result;
		}
		####Move pointer to beginning of file so that it will overwrite exiting data instead of starting at the end.
		fseek($handle, 0);
		$check1 = TRUE;
		$i = 0;
		while ($check1 !== 0) {
			$string = @str_pad($string, 4096, $string);
			$string = @str_split($string, 64);
			$previous_item = $string[0];
			foreach ($string as &$item){
				$item = @hash('sha256', $previous_item);
				$previous_item = $item;
			}
			unset($item);
			$string = @implode($string);
			$check1 = @fwrite($handle, $string, 4096);
			$i++;
		}
		fclose($handle);
		unset($handle);
		###Read from device 4096 bytes (4K sector size) at a time
		$handle = @fopen($source, 'r');
		if ($handle === FALSE) {
			$errors[] = 'handle';
			goto result;
		}
		$i = 0;
		$blocks_passed = 0;
		$blocks_failed = 0;
		while (@feof($handle) === FALSE) {
			$i++;
			$data = @fread($handle, 4096);
			$string2 = @str_pad($string2, 4096, $string2);
			$string2 = @str_split($string2, 64);
			$previous_item = $string2[0];
			foreach ($string2 as &$item){
				$item = @hash('sha256', $previous_item);
				$previous_item = $item;
			}
			unset($item);
			$string2 = @implode($string2);
			if ($string2 != $data){
				$blocks_failed++;
			} else {
				$blocks_passed++;
			}
		}
		fclose($handle);
		###Check if last block is empty, as this indicates full device was written to, and last block fail can be ignored.
		$check2 = FALSE;
		if ($data == ''){
			$check2 = TRUE;
			$blocks_failed--;
			$last_block_matches = 4096;
		} else {
			$check2 = FALSE;
		}
		###Check for partial match of last block.
		if ($check2 === FALSE){
			$data = str_split($data, 1);
			$string2 = str_split($string2, 1);
			$i2 = 0;
			foreach ($string2 as &$item){
				if ($item == $data[$i2]){
					$i2++;
				} else {
					$last_block_matches = $i2;
				}
			}
		}
		###Create report
		$device_size_in_bytes = $i * 4096;
		$device_size_in_bytes = $device_size_in_bytes - 4096;
		if ($check2 === FALSE){
			$device_size_in_bytes = $device_size_in_bytes + $last_block_matches;
		}
		$report = "Blocks:{$i} (4096 bytes per block)\nBlocks Passed:{$blocks_passed}\nBlocks Failed:{$blocks_failed}\nLast Block: {$last_block_matches} bytes of 4096 bytes match\nDrive Capacity:{$device_size_in_bytes} bytes";
	}
	result:
	##Display Errors
	if ($display_errors === TRUE){
		if (@empty($errors) === FALSE){
			$message = @implode(", ", $errors);
			if (function_exists('ritchey_verify_drive_i1_v1_format_error') === FALSE){
				function ritchey_verify_drive_i1_v1_format_error($errno, $errstr){
					echo $errstr;
				}
			}
			set_error_handler("ritchey_verify_drive_i1_v1_format_error");
			trigger_error($message, E_USER_ERROR);
		}
	}
	##Return
	if (@empty($errors) === TRUE){
		return $report;
	} else {
		return FALSE;
	}
}
}
?>